import React, { Component } from 'react'

class Counter extends Component {

  state = {
    value : this.props.value,
  }

  // User Experience
  getBadgeClasses() {
    let classes = "badge m-2 bg-"
    classes += this.state.value === 0 ? "warning" : "primary"
    return classes
  }

  // Fuctionality

  formatCount(){
    const { value } = this.state
    return value === 0 ? "Zero" : value
  }

  handleIncrement = product => {
    console.log(product);
    this.setState({ value : this.state.value + 1 })
  }

  render(){
    console.log("props", this.props)
    return (
      <div>
        <span className={ this.getBadgeClasses() }>{ this.formatCount() }</span>
        <button
          onClick={ (product) => this.handleIncrement(product)}
          className="btn btn-secondary btn-sm"  
        >
          increment
        </button>
      </div>
    )
  }

}

export default Counter